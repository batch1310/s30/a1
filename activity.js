const express = require('express');
const app = express();
const PORT = 4000;
const mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Schema
const userSchema = new mongoose.Schema(
	{
		name: String,
		status: {
			type: String,
			default: "pending"
		}
	}
);

//Models
const User = mongoose.model("User", userSchema);

//Routes
app.post("/signup", (req, res) => {
	User.create([{name: 'john'}, {password: 'johndoe'}]).save();
});


app.listen(PORT, () => console.log(`Server running at port ${PORT}`))